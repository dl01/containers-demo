import { expect } from 'chai';

import 'mocha';

import * as request from 'request-promise';

import { run, close } from '../';

describe('visitedUrls', () => {

    before(async () => {
        // start the webserver
        await run();
    });

    it('should record guest book entry', async () => {
        // test the guestbook
        expect(JSON.parse(await request('http://localhost:3700/mysupertest')).visitedUrls[0])
            .to.have.property('url', '/mysupertest');

        expect(JSON.parse(await request('http://localhost:3700/mysupertest2')).visitedUrls[1])
            .to.have.property('url', '/mysupertest');
    });

    after(() => {
        // stop the webserver
        close();
    })
})
