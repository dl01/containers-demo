import * as http from 'http';

import * as _ from 'lodash';

import { MongoClient } from 'mongodb';

let webserver: http.Server|null = null;
let mongoClient: MongoClient|null = null;

/// Starts a simple server which writes visited URLs to MongoDB
export async function run() {

    mongoClient = await MongoClient.connect(process.env.MONGO_URL || 'mongodb://localhost:27017',
        { useNewUrlParser: true, useUnifiedTopology: true });

    const db = mongoClient.db('test');

    webserver = http.createServer(async function (req, res) {

        await db.collection('urls').save(
            {_id: req.url, url: req.url, date: Date.now()});


        let urls = await db.collection('urls').find().sort({date: -1}).toArray();

        res.setHeader('Content-Type', 'application/json');

        res.end(JSON.stringify({
            visitedUrls: urls
        }));
    })

    webserver.listen(3700,
        () => console.log('Server started on port 3700 (http://localhost:3700/testme)'));
}

export function close() {
    if(webserver)
        webserver.close();

    if(mongoClient)
        mongoClient.close();
}

if(module === require.main) {
    try {
        run();
    }
    catch(err) {
        console.error('Could not start server:', err);
    }
}
