# this should (almost always) be the first line in any Dockerfile
# in this case, we are building our software on top of an official image
# bundled with NodeJS.
# For this example, `node` indicates we want a NodeJS bundled image
# the part after the `:` is a tag. `12-alpine` indicates the version and base OS
FROM node:12-alpine

# `apk` is the package manager for the alpine linux distribution. Here we install OS dependencies
RUN apk add --no-cache git tini

# Like `cd`, sets the base for all future layers
WORKDIR /usr/src

# docker is built in layers: it is good idea to install more static dependencies
# before dynamic code changes
# for python, this would be `COPY requirements.txt`
COPY package.json package-lock.json ./

# install dependencies
# for python this would be `pip install -r requirements.txt`
RUN npm i -production

# copy the rest of the app into the current directory
COPY ./dist ./

# metadata: what port will your app listen on?
EXPOSE 3700

# sets the Linux OS user within the container the app should run as
USER node

# read here for difference between ENTRYPOINT and CMD:
# https://www.ctl.io/developers/blog/post/dockerfile-entrypoint-vs-cmd/
# we use `tini` as the entrypoint here because it provides proper init
# functionality to our process tree
ENTRYPOINT ["/sbin/tini", "--"]

# the command to run if not overridden
CMD [ "/usr/local/bin/node", "index.js" ]
