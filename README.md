Docker Demo
====

Welcome to this simple demo repository. This project
includes a simple NodeJS Typescript project which reads URLs accessed
by an API and saves a record of the visit to a MongoDB database. Before
deployment, the Typescript has to be compiled into plain Javascript.
It uses GitLab CI to build, test, and package the app using containers.
The app is packaged into a container on the last step of the build,
ready for deployment to an orchestrator like Kubernetes.

## Included Files

| Filename | Description  |
|:-:|:-:|
| **index.ts**           | Main application script in TypeScript                        |
| **package.json**       | Contains NodeJS application dependencies                     |
| **package-lock.json**  | Prevents package.json dependencies from being inconsistent   |
| **docker-compose.yml** | Docker orchestration for local development                   |
| **Dockerfile**         | Packages application and dependencies into Alpine container  |
| **Dockerfile.big**     | Package application and dependencies into Ubuntu container   |
| **.gitlab-ci.yml**     | CI Pipeline Definition for GitLab                            |
| **test**               | Contains a sample integration test, runnable with `npm test` |

## Development Usage

Ensure all tools are installed on your computer:

* [`docker`](https://docs.docker.com/install/)
* [`docker-compose`](https://docs.docker.com/compose/install/)
* [`npm` and `node`](https://nodejs.org/en/)

Install local dependencies:

```
npm install
```

Then, start the whole stack from the root of the repository:

```bash
npm run build
```

If you make a code change, re-run this command.

The source for this command can be found in `package.json`. Under the hood, it is mostly
just running `docker-compose`

## Run Tests

After following the installation instructions above, running tests is easy:

```bash
npm test
```
